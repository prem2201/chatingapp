import axios from "axios";
import React from "react";

class Apps extends React.Component {
  constructor() {
    super();
    this.state = {
      dogs: [],
    };
  }
  componentDidMount() {
    axios
      .get("https://demo4865163.mockable.io/MOVIES")
      .then((res) => {
        console.log(res);
        this.setState({ dogs: res.data.message });
      })
      .catch((err) => console.log(err));
  }

  render() {
    return (
      <div className="App">
        {this.state.dogs.map((dog, index) => (
          <div key={index}>
            <img src={dog.photo} />
            <video
              style={{ position: "relative" }}
              width="300"
              height="240"
              controls
              poster={dog.photo}
            >
              <source src={dog.video} type="video/mp4" />
            </video>

            <p>{dog.name}</p>
          </div>
        ))}
      </div>
    );
  }
}
export default Apps;
