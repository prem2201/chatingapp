import Backdrop from "@material-ui/core/Backdrop";
import Button from "@material-ui/core/Button";
import Fade from "@material-ui/core/Fade";
import Grid from "@material-ui/core/Grid";
import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  media: {
    width: "80%",
    objectFit: "cover",
    height: 310,
  },
  btn: {
    backgroundColor: "white",
  },
  header: {
    flexGrow: 1,
    color: "white",
    padding: 10,
    fontWeight: "bold",
    backgroundColor: "black",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  input: {
    borderBottom: "1px solid white",
  },
  hover: {
    cursor: "pointer",
  },
  video: {
    [theme.breakpoints.down("sm")]: {
      height: 300,
      width: 300,
    },
    [theme.breakpoints.down("md")]: {
      height: 400,
      width: 400,
    },
    [theme.breakpoints.down("lg")]: {
      height: 500,
      width: 600,
    },
  },
  padd: {
    padding: 10,
    backgroundColor: "white",
    width: 250,
    cursor: "pointer",
    boxShadow:
      "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
    textAlign: "center",
  },
}));

const Moviecomponent = (props) => {
  const classes = useStyles();

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const { name, photo, video,poster} = props.movie;
  const [open, setOpen] = React.useState(false);
  return (
    <div>
      {" "}
      <div>
        <center>
          <Button
            variant="contained"
            className={classes.btn}
            disableElevation
            onClick={handleOpen}
            size="small"
          >
            <img
              src={photo}
              className={classes.media}
              alt={name}
              style={{
                border: "7px solid black",
              }}
            />
          </Button>
          <br />
          <b>{name}</b>
          <br />
        </center>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={open}>
            <Grid container>
              <Grid item xs={12}>
                <center>
                  <video width="400" controls poster={poster}>
                <source src={video} type="video/mp4" />
              </video>
                </center>
              </Grid>
            </Grid>
          </Fade>
        </Modal>
      </div>
    </div>
  );
};
export default Moviecomponent;
