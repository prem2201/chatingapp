import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { AiFillCloseCircle } from "react-icons/ai";

const API_KEY = "a9118a3a";
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: 20,
    backgroundColor: "white",
    boxShadow:
      "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
  },
  media: {
    height: 362,
    objectFit: "cover",
  },
  para: {
    fontWeight: "bold",
    color: "black",
    marginLeft: 20,
    textTransform: "capitalize",
  },
  para1: {
    fontWeight: "bold",
    color: "black",
    marginLeft: 20,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  padd: {
    padding: 10,
    width: 250,
    boxShadow:
      "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
    textAlign: "center",
  },
  btn: {
    float: "right",
    zIndex: 1,
  },
  close: {
    color: "red",
  },
}));

const MovieInfocomponent = (props) => {
  const classes = useStyles();
  const [movieInfo, setMovieInfo] = useState();
  const { selectedMovie } = props;

  useEffect(() => {
    axios
      .get(`https://www.omdbapi.com/?i=${selectedMovie}&apikey=${API_KEY}`)
      .then((response) => {
        setMovieInfo(response.data);
      });
  }, [selectedMovie]);
  return (
    <div className={classes.root}>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <IconButton
            className={classes.btn}
            onClick={() => props.onMovieSelect()}
          >
            <AiFillCloseCircle className={classes.close} />
          </IconButton>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={2}></Grid>
        <Grid item xs={12} sm={12} md={6} lg={5}></Grid>
      </Grid>
    </div>
  );
};
export default MovieInfocomponent;
