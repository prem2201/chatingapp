import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import Axios from "axios";
import React, { useEffect, useState } from "react";
import { MdLocalMovies } from "react-icons/md";
import MovieComponent from "./components/moviecomponent";

export const API_KEY = "a9118a3a";
const theme = createTheme({
  typography: {
    h5: {
      fontSize: 16,
      textAlign: "center",

      fontWeight: "bold",
    },
    h4: {
      fontWeight: "bold",
      color: "white",
    },
  },
});
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: 18,
    backgroundColor: "black",
    position: "sticky",
    top: 0,
    zIndex: 1,
  },
  search: {
    marginTop: 6,
    marginRight: 14,
    color: "white",
    fontSize: 30,
  },
  header: {
    flexGrow: 1,
    color: "white",
    padding: 10,
    fontWeight: "bold",
    backgroundColor: "black",
  },
  heading: {
    color: "white",
    fontSize: 30,
    fontWeight: "bold",
    marginLeft: 10,
    marginTop: -10,
  },
  logo: {
    color: "white",
    fontSize: 35,
    fontWeight: "bold",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  inputs: {
    border: "none",
    width: "100%",
    outline: "none",
    padding: 12,
    borderRadius: 8,
  },
  hover: {
    cursor: "pointer",
  },
  home: {
    padding: 20,
  },
  nosearch: {
    textAlign: "center",
    color: "white",
    textTransform: "capitalize",
    marginTop: "5%",
  },
}));

function App() {
  const classes = useStyles();

  const [movieList, updateMovieList] = useState([]);

  useEffect(() => {
    Axios.get("https://61079f3ad73c6400170d3549.mockapi.io/movie/movies")
      .then((res) => {
        updateMovieList(res.data);
      })
      .catch((err) => console.log(err));
  });

  return (
    <div>
      <ThemeProvider theme={theme}>
        <div className={classes.root}>
          <Grid container>
            <Grid item xs={12}>
              <Box display="flex" flexDirection="row">
                <Box>
                  <MdLocalMovies className={classes.logo} />
                </Box>
                <Box>
                  <span className={classes.heading}>CINEMA BOX</span>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </div>
        {/* {selectedMovie && (
          <MovieInfoComponent
            selectedMovie={selectedMovie}
            onMovieSelect={onMovieSelect}
          />
        )} */}
        <div className={classes.home}>
          <Grid container>
            {movieList?.length ? (
              movieList.map((movie, index) => (
                <Grid
                  key={index}
                  item
                  xs={12}
                  sm={6}
                  md={4}
                  lg={3}
                  className={classes.gri}
                >
                  <div>
                    <MovieComponent movie={movie} />
                  </div>
                </Grid>
              ))
            ) : (
              <Grid item xs={12}>
                {/* <Apps /> */}
                <h1>loading ...</h1>
              </Grid>
            )}
          </Grid>
        </div>
      </ThemeProvider>
    </div>
  );
}

export default App;
